//map Method
//creates a new array from calling a function for every array element.
//does not change the original array.

const numbers = [1, 2, 3, 4, 5]
const doubleNumbers = numbers.map(x => x * 2)
console.log(doubleNumbers)

//slice Method
//The slice method extracts a portion of an array and returns it as a new array without modifying the original array
//start index - end-1
const fruits = ['apple','banana','orange','grape']
const selectedFruits = fruits.slice(1,3)
console.log(selectedFruits)

//splice Method
//The splice method changes the contents of an array by removing or replacing existing 
//elements and/or adding new elements in place
//array.splice(index, howmany, item1, ....., itemX)

const colors = ['red','green','blue','yellow']
colors.splice(1, 2, 'purple', 'orange')
console.log(colors) // Output: ['red', 'purple', 'orange', 'yellow']

//reduce Method
//The reduce method reduces an array to a single value by applying a provided function to 
//each element and accumulating the result
//array.reduce(function(total, currentValue, currentIndex, arr), initialValue)

const numbers1 = [1, 2, 3, 4, 5]
const sum = numbers1.reduce((total, currentValue) => total + currentValue, 0);
console.log(sum); // Output: 15


//sort Method:
//The sort method sorts the elements of an array in place, either using the default sorting order or by a provided comparator function.
//array.sort(compareFunction)

const fruits1 = ['apple',  'orange','banana', 'grape'];
fruits1.sort();
console.log(fruits1); // Output: ['apple', 'banana', 'grape', 'orange']


//filter Method:
//The filter method creates a new array with elements that pass a provided function's test.

const numbers2 = [1, 2, 3, 4, 5];
const evenNumbers = numbers2.filter(x => x % 2 === 0);
console.log(evenNumbers); // Output: [2, 4]
