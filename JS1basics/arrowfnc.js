// Arrow Functions:
// Rewrite a simple function (e.g., multiplication) using arrow function syntax.

//Original Function:

function multiply(a, b) {
    return a * b;
  }
  
  // Example usage
  const result1 = multiply(5, 7);
  console.log(result1); // Output: 35


  
 // Arrow Function:

 // Arrow function syntax
const multiply = (a, b) => a * b;

// Example usage
const result = multiply(5, 7);
console.log(result); // Output: 35