//   Asynchronous Programming (async/await):
// Create a function that simulates fetching data from an API using setTimeout. Refactor the function to use async/await syntax to make it more readable.

//Without async/await:
function fetchDataFromApi(callback) {
    // Simulate API call with setTimeout
    setTimeout(() => {
      const data = { message: 'Data fetched successfully Without async/await!' }
      callback(null, data);
    }, 2000); // Simulating a delay of 2000 milliseconds (2 seconds)
  }
  
  // Example usage
  fetchDataFromApi((error, data) => {
    if (error) {
      console.error('Error:', error);
    } else {
      console.log('Success:', data);
    }
  });

   //--------------------------------------------
   console.log("\n")
   console.log("\n")
   //-------------------------------------------
 
 
  
  //With async/await:
  function fetchDataFromApiAsync() {
      return new Promise((resolve) => {
        // Simulate API call with setTimeout
        setTimeout(() => {
          const data = { message: 'Data fetched successfully With async/await!' };
          resolve(data);
        }, 2000); // Simulating a delay of 2000 milliseconds (2 seconds)
      });
    }
    
    // Refactored example usage using async/await
    async function fetchData() {
      try {
        const data = await fetchDataFromApiAsync();
        console.log('Success:', data);
      } catch (error) {
        console.error('Error:', error);
      }
    }
    
    // Call the fetchData function
    fetchData();
  