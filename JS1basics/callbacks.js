// Callbacks:
// Create a function processData that takes an array and a callback function. Apply the callback function to each element of the array and return the modified array.

function processData(array,callback){
    
    const modifiedArray = array.map(callback)
    
    return modifiedArray
}
const inputArray = [1, 2, 3, 4, 5]
// callback function
const doubleCallback = (element) => element * 2

const resultArray = processData(inputArray,doubleCallback)

console.log(resultArray)

//demonstrated doubling each element of the input array using a doubleCallback function