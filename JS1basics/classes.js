// Classes:
// Define a class Car with properties like make, model, 
// and a method start() that logs a message. Create an instance of the class and call the start() method.

// Define the Car class
class Car {
    constructor(make, model) {
      this.make = make;
      this.model = model;
    }
  
    start() {
      console.log(`The ${this.make} ${this.model} is now running.`);
    }
  }
  
  // Create an instance of the Car class
  const myCar = new Car('Tata', 'Nexon');
  
  // Call the start() method
  myCar.start();
  