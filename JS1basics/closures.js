//Closures:
//Write a function that returns another function.
//The inner function should capture and use a variable from the outer function's scope.
//Call the outer function and the returned inner function, and observe the behavior.

function one(){    //outer function
    
    let a=10                //outer variable
    console.log("From func 1 returning func 2 ")
    
    function two(){     //inner function
        console.log(a)  //capturing outer variable
        a=a+2
        console.log(a)  
        return "From func 2"
    }
    
    return two();
}

function main(){
     console.log(one())
    //console.log(two())    ---error
}

main()




  







  

  
  





