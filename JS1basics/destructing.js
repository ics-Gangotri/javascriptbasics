//   ES6 Destructuring:
// Create an object with information about a person (e.g., name, age, address). Use destructuring to extract and log specific properties.

// Create an object with information about a person
const person = {
    name: 'John Doe',
    age: 30,
    address: '123 Main Street, Cityville',
    
  }
  
  // Destructuring to extract and log specific properties
  const { name: personName, age: personAge, address: personadd } = person;

console.log(`Name: ${personName}`);
console.log(`Age: ${personAge}`);
console.log(`Address: ${personadd}`);