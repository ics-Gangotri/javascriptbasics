// Error Handling:
// Write a function that may throw an error. Use a try-catch block to handle the error and log a user-friendly message.

function divideNumbers(a, b) {
    if (b === 0) {
      throw new Error('Division by zero is not allowed.');
    }
  
    return a / b;
  }
  
  // Example usage with try-catch block
  try {

    const result = divideNumbers(10, 0);

    console.log('Result:', result); // This line won't be reached if an error occurs
    
  } catch (error) {
    console.error('Error:', error.message);
    // Log a user-friendly message when an error occurs
  }
  

