// Modules:
// Create two separate JavaScript files. In one file, define a function. In the other file, import and use that function.

// File 1: modules.js

// Define a function
function greet(name) {
    return `Hello, ${name}!`;
  }
  
  // Export the function
  module.exports = greet;
  