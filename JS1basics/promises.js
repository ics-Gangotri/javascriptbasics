// Promises:
// Write a function that returns a Promise. Resolve the Promise with a success message after a delay using setTimeout. Handle the Promise using .then() and .catch().

function delayedSuccessMessage(delay) {
    return new Promise((resolve, reject) => {
        
      // Simulate an asynchronous operation with setTimeout
      setTimeout(() => {
      resolve('Success! Operation completed after ' + delay + ' milliseconds') 
      }, delay)
      
    })
  }
  
  const delayTime = 2000; // 2000 milliseconds (2 seconds)
  
  delayedSuccessMessage(delayTime)
    .then((message) => {
      console.log(message);
    })
    .catch((error) => {
      console.error('Error:', error);
    })
    