// File 2: usingmodules.js

// Import the function from modules.js
const greet1 = require('./modules');

// Use the imported function
const greetingMessage = greet1('John');
console.log(greetingMessage); // Output: Hello, John!
